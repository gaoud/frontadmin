import {Component, Input, OnInit} from '@angular/core';
import {ConducteurserviceService} from '../service/conducteurservice.service';
import {AuthentificationService} from '../service/authentification.service';
import {Router} from '@angular/router';
import {FormBuilder, FormControl, FormGroup, NgForm, Validators} from '@angular/forms';
import {User} from '../model/user';
import {Conducteur} from '../model/conducteur';

import {any} from 'codelyzer/util/function';
import {ImageService} from '../service/image.service';
import {HttpEventType, HttpResponse} from '@angular/common/http';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-conducteur',
  templateUrl: './conducteur.component.html',
  styleUrls: ['./conducteur.component.css']
})


export class ConducteurComponent implements OnInit {
  term;
  p: number = 1;
  progress: { percentage: number } = {percentage: 0};
  nbrannonce1;
  photo;
  filesToUpload: Array<File>;
  lisconducteur;
  added = false;
  id;
  conducteur = new Conducteur();




  constructor(private conducteurserviceService: ConducteurserviceService, private imsr: ImageService) {
    this.all();

this.photo="choisir une image";
  }


  ngOnInit() {




  }


  all() {
    this.conducteurserviceService.getall().subscribe(res => {
      console.log(res);
      this.lisconducteur = res;


    });

  }

  delete(id) {
    Swal.fire({
      title: 'Are you sure?',
      text: 'You won\'t be able to revert this!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {

        this.conducteurserviceService.delete(id).subscribe(res => {

          this.all();

          Swal.fire(
            'Deleted!',
            'Your file has been deleted.',
            'success'
          );
        });
      }
    });
  }


  ajout() {
    const data = {
      username: this.conducteur.username,
      firstName: this.conducteur.firstName,
      lastName: this.conducteur.lastName,
      email: this.conducteur.email,
      birthdate: this.conducteur.birthdate,
      genre: this.conducteur.genre,
      tel: this.conducteur.tel,
      permis: this.conducteur.permis,
      cin: this.conducteur.cin,
      adress: this.conducteur.adress,

      password: this.conducteur.password,
      confirmedPassword: this.conducteur.confirmedPassword,
      photo: this.filesToUpload[0].name
    };


    this.conducteurserviceService.addconducteur(data).subscribe(res => {
      console.log(res);
      this.added = true;
      this.imsr.pushFileToStorage(this.filesToUpload[0]).subscribe(rest => {
        console.log(rest);
        // if (event.type === HttpEventType.UploadProgress) {
        //   this.progress.percentage = Math.round(100 * event.loaded / event.total);
        // } else if (event instanceof HttpResponse) {
        //   console.log('File is completely uploaded!');
        // }
      });
      this.conducteur = new Conducteur();
      this.conducteur.genre ='';
      this.photo = 'choisir une image';
      this.all();




    });

  }


  recuper(id, username, firstName, lastName, email, birthdate, genre, tel, permis, cin, adress, image, password, confirmedPassword) {

    this.id = id;
    this.conducteur.username = username;
    this.conducteur.firstName = firstName;
    this.conducteur.lastName = lastName;
    this.conducteur.email = email;
    this.conducteur.birthdate = birthdate;
    this.conducteur.genre = genre;
    this.conducteur.tel = tel;
    this.conducteur.permis = permis;
    this.conducteur.cin = cin;
    this.conducteur.adress = adress;

    this.photo = image;
    this.conducteur.password = password;
    this.conducteur.confirmedPassword = confirmedPassword;
    this.nbrannonce(this.id);

  }

  modifierconducteur() {
    const data = {
      username: this.conducteur.username,
      firstName: this.conducteur.firstName,
      lastName: this.conducteur.lastName,
      email: this.conducteur.email,
      birthdate: this.conducteur.birthdate,
      genre: this.conducteur.genre,
      tel: this.conducteur.tel,
      permis: this.conducteur.permis,
      cin: this.conducteur.cin,
      adress: this.conducteur.adress,


      password: this.conducteur.password,
      confirmedPassword: this.conducteur.confirmedPassword,
      photo: this.photo
    };

    console.log(data);


    this.conducteurserviceService.modif(this.id, data).subscribe(res => {
      console.log(res);

      if (this.filesToUpload != undefined) {

        this.imsr.pushFileToStorage(this.filesToUpload[0]).subscribe(rest => {
          console.log(rest);
        });
      }

      // if (event.type === HttpEventType.UploadProgress) {
      //   this.progress.percentage = Math.round(100 * event.loaded / event.total);
      // } else if (event instanceof HttpResponse) {
      //   console.log('File is completely uploaded!');
      // }

      this.all();
    });

  }

  recuperFile(file) {
    this.filesToUpload = <Array<File>> file.target.files;

    this.photo = file.target.files[0]['name'];
  }

  closemodal() {
 this.conducteur = new Conducteur();
    this.photo = 'choisir une image';

  }

  nbrannonce(idconducteur) {
    this.conducteurserviceService.nbrannonce(idconducteur).subscribe((res: any) => {
      console.log(res);
      this.nbrannonce1 = res.length;


    });

  }

}






