import {Component, Input, OnInit} from '@angular/core';
import {ConducteurserviceService} from '../../service';

@Component({
  selector: 'app-trajet',
  templateUrl: './trajet.component.html',
  styleUrls: ['./trajet.component.css']
})
export class TrajetComponent implements OnInit {

   @Input() id;

   nbre;
  constructor(private conducteurserviceService:ConducteurserviceService) { }

  ngOnInit() {
     this.nbrannonce(this.id);
  }


  nbrannonce(idconducteur) {
    this.conducteurserviceService.nbrannonce(idconducteur).subscribe((res: any) => {
      console.log(res);
      this.nbre = res.length;


    });

  }

}
