import { Component, OnInit } from '@angular/core';
import {ReclamationService} from '../service/reclamation.service';
import {Reclamation} from '../model/reclamation';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Mail} from '../model/mail';
import {$COLON} from 'codelyzer/angular/styles/chars';

@Component({
  selector: 'app-reclamation',
  templateUrl: './reclamation.component.html',
  styleUrls: ['./reclamation.component.css']
})
export class ReclamationComponent implements OnInit {
to;
  term;
  id;
  etat= false;
  reclamationForm:FormGroup ;
  listreclamation;
  submitted = false;
  mail=new Mail();
  reclamtion = new Reclamation();
  constructor( private formBuilder: FormBuilder,private reclamtionservice: ReclamationService) {
    this.all();
  }


  ngOnInit() {
    this.reclamationForm = this.formBuilder.group({
      content: ['', [Validators.required]]});

  }

  get f() {
    return this.reclamationForm.controls;
  }

  sendMail() {

    this.submitted = true;
    const mail={
      from :this.mail.from="rimesbenabdallh@gmail.com",
     to: this.mail.to=this.to,
      subject:this.mail.subject="Reponse pour votre Reclamation",
      content: this.mail.content=this.reclamationForm.value["content"]
  }
    console.log(this.mail);
    this.reclamtionservice.sendmail(mail).subscribe(res => {
        console.log(res);

      }
    );

  }


  all() {

    this.reclamtionservice.getall().subscribe(res => {
        console.log(res);
      this.listreclamation=res
      }
    );
  }



  recuper(id,titre, sujet, reponseRec) {

    this.id = id;
    this.reclamtion.titre= titre;
    this.reclamtion.sujet = sujet;
    this.reclamtion.reponseRec = reponseRec;



  }

getonreclamation(id){
    this.reclamtionservice.getOne(id).subscribe( (data:any)=>{
      console.log(data);
      this.reclamtion=data;
      this.to=data.appUser.email;

    })
}

reponseenvouyer(id){
    this.reclamtionservice.reponse(id).subscribe(data=>{
      this.etat=true
      console.log(data)})
}

  deletereclamation(id) {
    this.reclamtionservice.delete(id).subscribe(data => {
      this.all()
    })
  }
}
