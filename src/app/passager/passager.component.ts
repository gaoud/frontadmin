import { Component, OnInit } from '@angular/core';
import {Conducteur} from '../model/conducteur';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthentificationService, ConducteurserviceService} from '../service';
import {Router} from '@angular/router';
import {PassagerService} from '../service/passager.service';
import {Passager} from '../model/passager';
import Swal from "sweetalert2";

@Component({
  selector: 'app-passager',
  templateUrl: './passager.component.html',
  styleUrls: ['./passager.component.css']
})
export class PassagerComponent implements OnInit {
  term;
  p: number = 1;
  listepassager;



  submitted = false;


passager = new Passager()

  addForm: FormGroup ;
  id;


  constructor(private passagerservice: PassagerService , private  router: Router,private formBuilder:FormBuilder) {
    this.all()
    this.recuper1(this.id)
  }

  ngOnInit() {

  }






  all(){
    this.passagerservice.getall().subscribe(res=>{
      console.log(res)
      this.listepassager=res;
    })

  }
  removepassager(id) {



      Swal.fire({
        title: 'Vous  êtes sûre?',
        text: "Vous ne pourrez pas revenir en arrière!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Oui, Supprimer!'
      }).then((result) => {
        if (result.value) {

          this.passagerservice.delete(id).subscribe(res => {

            this.all();

            Swal.fire(
              'Suprimer!',
              'Passager Bien Supprimer',
              'success'
            )
          });
        }
      })
    }

  recuper(id,username, firstName, lastName, email, tel, genre,password,confirmedPassword){
    this.id=id;
this.passager.username=username,
    this.passager.firstName = firstName;
    this.passager.lastName = lastName;
    this.passager.email = email;


    this.passager.tel = tel;
    this.passager.genre = genre;
    this.passager.password = password;
    this.passager.confirmedPassword = confirmedPassword;
  }
recuper1(id){
    this.id=id
}
  addpassager() {

    this.submitted = true;

    // const data1= {
    //
    //   username: this.passager.username,
    //   firstName: this.passager.firstName,
    //   lastName: this.passager.lastName,
    //   email: this.passager.email,
    //   tel: this.passager.tel,
    //   genre: this.passager.genre,
    //
    //   password: this.passager.password,
    //   confirmedPassword: this.passager.confirmedPassword}
    this.passagerservice.addpassager(this.passager).subscribe(res=>{
        console.log(res);
        this.all()
      }

    )
  }

  modifierpassagerr() {



    this.passagerservice.modifpassager(this.id,this.passager).subscribe(res => {
      console.log(res);
      this.all()
    });

  }
  closemodal() {
    this.passager.username = '';
    this.passager.firstName = '';
    this.passager.lastName = '';
    this.passager.email = '';

    this.passager.genre = '';
    this.passager.tel = '';


    this.passager.password = '';
    this.passager.confirmedPassword = '';

  }


}







