import {Component, Input, OnInit} from '@angular/core';
import {ConducteurserviceService} from '../../service';
import {PassagerService} from '../../service/passager.service';

@Component({
  selector: 'app-reservation',
  templateUrl: './reservation.component.html',
  styleUrls: ['./reservation.component.css']
})
export class ReservationComponent implements OnInit {

   @Input() id;

  nbrereservation;
  constructor(private passagerService :PassagerService) { }

  ngOnInit() {
     this.nbrreservation(this.id);
  }


  nbrreservation(idpassager) {
    this.passagerService.nbrreservation(idpassager).subscribe((res: any) => {
      console.log(res);
      this.nbrereservation = res.length;


    });

  }

}
