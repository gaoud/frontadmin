import { Component, OnInit } from '@angular/core';
import {ConducteurserviceService} from '../../service';
import {ResrvationService} from '../../service/resrvation.service';
import {AnnonceService} from '../../service/annonce.service';
import {PassagerService} from '../../service/passager.service';

@Component({
  selector: 'app-annonce',
  templateUrl: './annonce.component.html',
  styleUrls: ['./annonce.component.css']
})
export class AnnonceComponent implements OnInit {
  nbrannonce;
  nberreservation;
  listannonce;
  listreservation;

  chart ;
  constructor(private conducteurservice:ConducteurserviceService,private reservationservice:ResrvationService,private annonceservice:AnnonceService,private passagerservice:PassagerService) {

  }

  public barChartOptions: any = {
  scaleShowVerticalLines: false,
  responsive: true
};
  public barChartLabels: string[] = ['jan', 'fev', 'mars', 'avril', 'mai', 'juin', 'juillet', 'aout', 'sep', 'oct', 'nvb', 'dcm'];
  public barChartType = 'bar';
  public barChartLegend = true;
  public barChartType1 = 'line';
  public barChartData: any[] = [
    {data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ], label: 'Annonce publier '},


  ];
  public barChartData1: any[] = [
    {data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ], label: 'Reservation '},


  ];
  ngOnInit() {
    this.allannonce();
    this.allreservation();
  }
  allannonce(){
    let clone = JSON.parse(JSON.stringify(this.barChartData));
    return this.annonceservice.getall().subscribe((res:any)=>{
      console.log(res);
      this.listannonce=res;
      this.nbrannonce=res.length;
      for (var i =0;i<this.listannonce.length;i++) {
        for (var j = 0; j<12 ;j++){
          var d = new Date(this.listannonce[i].date_depart);

          if(d.getMonth()== j)
            clone[0].data[j]= clone[0].data[j]+1;
        }
      }
      this.barChartData =clone;
    })

  }
  allreservation(){
    const clone = JSON.parse(JSON.stringify(this.barChartData1));
    return this.reservationservice.getall().subscribe((res:any)=>{
      console.log(res);
      this.listreservation=res;
      this.nberreservation=res.length;



      for (let i = 0; i < this.listreservation.length; i++) {
        for (let j = 0; j < 12 ; j++) {
          let d = new Date(this.listreservation[i].heure);
          console.log(d);
          console.log(d.getMonth());
          if (d.getMonth() === j) {
            clone[0].data[j] = clone[0].data[j] + 1;
          }
        }
      }
      this.barChartData1 = clone;
    });

    }

  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }

  public randomize(): void {
    // Only Change 3 values
    const data = [
      Math.round(Math.random() * 100),
      59,
      80,
      (Math.random() * 100),
      56,
      (Math.random() * 100),
      40];
    const clone = JSON.parse(JSON.stringify(this.barChartData));
    clone[0].data = data;
    this.barChartData = clone;
    /**
     * (My guess), for Angular to recognize the change in the dataset
     * it has to change the dataset variable directly,
     * so one way around it, is to clone the data, change it and then
     * assign it;
     */
  }
}
