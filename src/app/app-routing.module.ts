import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './home/home.component';

import {FooterComponent} from './home/footer/footer.component';
import {SideBarComponent} from './home/side-bar/side-bar.component';
import {ContainerComponent} from './home/container/container.component';
import {NavbarComponent} from './home/navbar/navbar.component';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {Erreur404Component} from './erreur404/erreur404.component';
import {ConducteurComponent} from './conducteur/conducteur.component';
import {PassagerComponent} from './passager/passager.component';
import {AuthGuard} from './guard/auth.guard';

import {ReclamationComponent} from './reclamation/reclamation.component';

import {MarquevoitureComponent} from './marquevoiture/marquevoiture.component';
import {ProfileadminComponent} from './profileadmin/profileadmin.component';
import {ProfilUserComponent} from './profileadmin/profil-user/profil-user.component';
import {AnnonceComponent} from './statstique/annonce/annonce.component';



const routes: Routes = [
  {path:'',component:LoginComponent },
  {path:'register',component:RegisterComponent},

  { path: 'home', component: HomeComponent , children:[{path:'',component:ContainerComponent},
      {path:'conducteur',component:ConducteurComponent},

      {path:'reclamation',component: ReclamationComponent},
      {path:'passager',component:PassagerComponent},

      {path:'voiture',component:MarquevoitureComponent},
      {path:'profileadmin',component:ProfileadminComponent},
      {path:'profiluser/:id',component:ProfilUserComponent},
      {path:'statstique',component:AnnonceComponent},

      ],canActivate:[AuthGuard] ,canDeactivate: [AuthGuard],},
  { path:"**",component:Erreur404Component }


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
