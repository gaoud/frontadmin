import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AuthentificationService} from './authentification.service';

@Injectable({
  providedIn: 'root'
})
export class ResrvationService {
  basurl = "http://localhost:9001";
  constructor(private http: HttpClient, private authService:AuthentificationService) { }



  getall() {

    let headers= new HttpHeaders({'authorization':'Bearer '+ this.authService.jwt});

    return this.http.get(this.basurl + '/res/reservations',{headers:headers});

  }
  deleteresrvation(id) {
    let headers= new HttpHeaders({'authorization':'Bearer '+ this.authService.jwt});


    return this.http.delete(this.basurl + '/res/deleteresrvation/'+id,{headers:headers});


  }

  addreservation(idannonce,idpassager,data) {
    let headers= new HttpHeaders({'authorization':'Bearer '+ this.authService.jwt});
    return this.http.post( this.basurl + '/res/addreservation/'+idannonce+'/'+idpassager,data,{headers:headers});


  }
  getone(id) {

    let headers = new HttpHeaders({'Authorization': 'Bearer ' + this.authService.jwt});
    return this.http.get(this.basurl + '/res/reservations/'+id, {headers: headers});


  }
}
