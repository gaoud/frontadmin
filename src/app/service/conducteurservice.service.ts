import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AuthentificationService} from './authentification.service';
import {Conducteur} from '../model/conducteur';

@Injectable({
  providedIn: 'root'
})
export class ConducteurserviceService {

  basurl = 'http://localhost:9001';

  constructor(private http: HttpClient, private authService: AuthentificationService) {
  }

  getall() {

    let headers = new HttpHeaders({'authorization': 'Bearer ' + this.authService.jwt});

    return this.http.get(this.basurl + '/chauffeur/all', {headers: headers});

  }

  delete(id) {
    let headers = new HttpHeaders({'authorization': 'Bearer ' + this.authService.jwt});


    return this.http.delete(this.basurl + '/chauffeur/delete/' + id, {headers: headers});


  }

  addconducteur(c) {
    
    let headers = new HttpHeaders({'Authorization': 'Bearer ' + this.authService.jwt});
    return this.http.post(this.basurl + '/chauffeur/add', c, {headers: headers});


  }


  modif(id, c) {
    let headers = new HttpHeaders({'authorization': 'Bearer ' + this.authService.jwt});
    return this.http.put(this.basurl + '/chauffeur/edit/'+id, c, {headers: headers});
  }



  nbrannonce(idC) {

    let headers = new HttpHeaders({'Authorization': 'Bearer ' + this.authService.jwt});
    return this.http.get(this.basurl + '/chauffeur/allAnnoncebyxhaufeur/'+idC, {headers: headers});


  }
  getbyid(id){
    let headers = new HttpHeaders({'Authorization': 'Bearer ' + this.authService.jwt});
    return this.http.get(this.basurl + '/chauffeur/one/'+id, {headers: headers});

  }
}

