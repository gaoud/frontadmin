import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AuthentificationService} from './authentification.service';

@Injectable({
  providedIn: 'root'
})
export class VoitureService {

  basurl = "http://localhost:9001";

  constructor(private http: HttpClient, private authService:AuthentificationService) {
  }

  getall() {

    let headers= new HttpHeaders({'authorization':'Bearer '+ this.authService.jwt});

    return this.http.get(this.basurl + '/marque/all',{headers:headers});

  }

  delete(id) {
    let headers= new HttpHeaders({'authorization':'Bearer '+ this.authService.jwt});


    return this.http.delete(this.basurl + '/marque/delete/'+id,{headers:headers});



  }

  addmarque(data) {
    let headers= new HttpHeaders({'authorization':'Bearer '+ this.authService.jwt});
    return this.http.post( this.basurl + '/marque/add',data,{headers:headers});


  }
  modifmarque(id,c) {
    let headers = new HttpHeaders({'authorization': 'Bearer ' + this.authService.jwt});
    return this.http.put(this.basurl + '/marque/edit/'+id,c,{headers: headers});

  }
  getOnemarque(id) {
    let headers = new HttpHeaders({'authorization': 'Bearer ' + this.authService.jwt});
    return this.http.get(this.basurl + '/marque/one/'+id, {headers: headers});

  }





  getallmodel() {

    let headers= new HttpHeaders({'authorization':'Bearer '+ this.authService.jwt});

    return this.http.get(this.basurl + '/model/all',{headers:headers});

  }
  deletemodel(id) {
    let headers= new HttpHeaders({'authorization':'Bearer '+ this.authService.jwt});


    return this.http.delete(this.basurl + '/model/delete/'+id,{headers:headers});


  }

  addmodel(idG,data) {
    let headers= new HttpHeaders({'authorization':'Bearer '+ this.authService.jwt});
    return this.http.post( this.basurl + '/model/add/'+ idG,data,{headers:headers});


  }
  modifmodel(id, idG) {
    let headers = new HttpHeaders({'authorization': 'Bearer ' + this.authService.jwt});
    return this.http.put(this.basurl + '/model/update/'+id,idG,{headers: headers});

  }
  getOnemodel(id) {
    let headers = new HttpHeaders({'authorization': 'Bearer ' + this.authService.jwt});
    return this.http.get(this.basurl + '/model/one/'+id, {headers: headers});

  }
  getmodelbymarque(idmodel){
    let headers = new HttpHeaders({'authorization': 'Bearer ' + this.authService.jwt});
    return this.http.get(this.basurl + '/model/modelbymarque/'+idmodel, {headers: headers});


  }

}
