import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {JwtHelperService} from '@auth0/angular-jwt';
import {User} from '../model/user';
import {Observable} from 'rxjs';
import {Admin} from '../model/admin';

@Injectable({
  providedIn: 'root'
})
export class AuthentificationService {
  basurl = 'http://localhost:9001';
  jwt: string;
  username: string;
  roles: Array<String>;

  constructor(private http: HttpClient) {

  }

  login(data) {

    return this.http.post(this.basurl + '/login', data, {observe: 'response'});

  }

  register(data) {
    let headers = new HttpHeaders({'authorization': 'Bearer ' + this.jwt});
    return this.http.post(this.basurl + '/admin/add', data, {headers: headers});


  }

  getprofile(): Observable<Admin> {
    let headers = new HttpHeaders({'authorization': 'Bearer ' + this.jwt});

    return this.http.get<Admin>(this.basurl + '/user/byiduser', {headers: headers});
  }

  parseJWT() {

    const jwtHelper = new JwtHelperService();
    const objJWT = jwtHelper.decodeToken(this.jwt);
    this.username = objJWT.obj;
    this.roles = objJWT.roles;
  }

  saveToken(jwt: string) {
    sessionStorage.setItem('token', jwt);
    this.jwt = jwt;
    this.parseJWT();
  }

  isAdmin() {
    return this.roles.indexOf('ADMIN') >= 0;
  }

  isConducteur() {
    return this.roles.indexOf('CONDUCTEUR') >= 0;

  }

  isPassager() {
    return this.roles.indexOf('PASSAGER') >= 0;

  }

  isAuthenticated() {

    return this.roles && (this.isAdmin() || this.isPassager() || this.isConducteur());
  }

  loadToken() {
    this.jwt = sessionStorage.getItem('token');
    this.parseJWT();
  }

  logout() {
    sessionStorage.removeItem('token');
    this.initParams();

  }

  initParams() {
    this.jwt = undefined;
    this.username = undefined;
    this.roles = undefined;
  }

}
