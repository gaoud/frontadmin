import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AuthentificationService} from './authentification.service';

@Injectable({
  providedIn: 'root'
})
export class PassagerService {

  basurl = "http://localhost:9001";

  constructor(private http: HttpClient, private authService:AuthentificationService) {
  }

  getall() {

    let headers= new HttpHeaders({'authorization':'Bearer '+ this.authService.jwt});

    return this.http.get(this.basurl + '/passeger/all',{headers:headers});

  }

  getone(id) {

    let headers= new HttpHeaders({'authorization':'Bearer '+ this.authService.jwt});

    return this.http.get(this.basurl + '/passeger/one/'+id,{headers:headers});

  }
  delete(id) {
    let headers= new HttpHeaders({'authorization':'Bearer '+ this.authService.jwt});


    return this.http.delete(this.basurl + '/passeger/delete/'+id,{headers:headers});


  }

  addpassager(c) {
    let headers= new HttpHeaders({'authorization':'Bearer '+ this.authService.jwt});
    return this.http.post( this.basurl + '/passeger/add',c,{headers:headers});


  }

  modifpassager(id, c) {
    let headers = new HttpHeaders({'authorization': 'Bearer ' + this.authService.jwt});
    return this.http.put(this.basurl + '/passeger/update/'+id, c, {headers: headers});
  }



 nbrreservation(idP) {

   let headers = new HttpHeaders({'Authorization': 'Bearer ' + this.authService.jwt});
   return this.http.get(this.basurl + '/passeger/allreservationbyP/' + idP, {headers: headers});

 }
  }
