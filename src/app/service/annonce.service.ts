import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AuthentificationService} from './authentification.service';

@Injectable({
  providedIn: 'root'
})
export class AnnonceService {
  basurl = 'http://localhost:9001';

  constructor(private http: HttpClient, private authService: AuthentificationService) {
  }


  getall() {

    const headers = new HttpHeaders({authorization: 'Bearer ' + this.authService.jwt});

    return this.http.get(this.basurl + '/annonce/all', {headers:headers});

  }
  addannnce(idchaufeur,data) {
    let headers = new HttpHeaders({'authorization': 'Bearer ' + this.authService.jwt});

    return this.http.post( 'http://localhost:9001/annonce/add/'+idchaufeur,data, {headers:headers});


  }
  getone(id){
    let headers = new HttpHeaders({'authorization': 'Bearer ' + this.authService.jwt});

    return this.http.get( 'http://localhost:9001/annonce/one/'+id, {headers:headers});
  }
}
