import { Component, OnInit } from '@angular/core';
import {ConducteurserviceService} from '../../service/conducteurservice.service';
import {AuthentificationService} from '../../service/authentification.service';

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.css']
})
export class SideBarComponent implements OnInit {

  constructor( private authService:AuthentificationService) { }

  ngOnInit() {
  }

  isAdmin() {
    return this.authService.isAdmin();
  }
  isConducteur(){
    return this.authService.isConducteur();
  }

}
