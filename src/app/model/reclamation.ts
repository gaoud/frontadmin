import {User} from './user';

export class Reclamation {
  titre: string;
  sujet: string;
  reponseRec: boolean;
  appUser = new User();
}
