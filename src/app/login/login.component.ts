import { Component, OnInit } from '@angular/core';
import {AuthentificationService} from '../service/authentification.service';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {first} from 'rxjs/operators';
import {AlertService} from '../service/alert.service';
import {User} from '../model/user';
import {el} from '@angular/platform-browser/testing/src/browser_util';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  message: string;
  user = new User;
  loginForm: FormGroup;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenService: AuthentificationService,
    private alertService: AlertService) {


  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });


  }

  // convenience getter for easy access to form fields
  get f() {
    return this.loginForm.controls;
  }


  onlogin() {

    this.submitted = true;

    const data = {
      username: this.loginForm.value["username"],

      password: this.loginForm.value["password"],
    }

    this.authenService.login(data).subscribe(res => {
      console.log(res);

      if (res && res.status=== 200 ) {

        const jwt = res.headers.get('Authorization')
        this.authenService.saveToken(jwt);

        localStorage.setItem("token", jwt)
        this.router.navigate(['home']);

      } else {
        console.log("error")
        Swal.fire(
          'OPPs',
          'Vérifier vos coordonnées:)',
          'error'
        );
      }


    });
    //     const jwt = res.headers.get('Authorization')
    //     this.authenService.saveToken(jwt);
    //    localStorage.setItem("token",jwt)
    //      this.router.navigate(['home']);
    //
    //
    //   })
    // }


  }
}
