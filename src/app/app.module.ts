import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { FooterComponent } from './home/footer/footer.component';
import { ContainerComponent } from './home/container/container.component';
import { SideBarComponent } from './home/side-bar/side-bar.component';

import { LoginComponent } from './login/login.component';
import { NavbarComponent } from './home/navbar/navbar.component';
import { ConducteurComponent } from './conducteur/conducteur.component';
import { PassagerComponent } from './passager/passager.component';
import { Erreur404Component } from './erreur404/erreur404.component';
import { RegisterComponent } from './register/register.component';
import {NgxPaginationModule} from 'ngx-pagination';
import {FormGroup, FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MustMatchDirective} from './helpes/must-match-directive';

import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import { ChartsModule } from 'ng2-charts';
import {HttpClientModule} from '@angular/common/http';
import { DatePickerModule } from '@syncfusion/ej2-angular-calendars';
import { ReclamationComponent } from './reclamation/reclamation.component';
import { PassagerPipe } from "./recherche/passager.pipe";
import { ConducteurPipe } from './recherche/conducteur.pipe';
import { VoyagePipe } from './recherche/voyage.pipe';
import { PaiementComponent } from './paiement/paiement.component';


import { MarquevoitureComponent } from './marquevoiture/marquevoiture.component';
import { ProfileadminComponent } from './profileadmin/profileadmin.component';
import { TrajetComponent } from './conducteur/trajet/trajet.component';
import {ReservationComponent} from './passager/reservation/reservation.component';

import { ProfilUserComponent } from './profileadmin/profil-user/profil-user.component';
import {StarRatingModule} from 'angular-star-rating';
import { AnnonceComponent } from './statstique/annonce/annonce.component';
import {CalendarModule, DateAdapter} from 'angular-calendar';
import {adapterFactory} from 'angular-calendar/date-adapters/date-fns';
import {FlatpickrModule} from 'angularx-flatpickr';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgbModalModule} from '@ng-bootstrap/ng-bootstrap';



@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    FooterComponent,
    ContainerComponent,
    SideBarComponent,
    LoginComponent,
    NavbarComponent,
    ConducteurComponent,
    PassagerComponent,
    Erreur404Component,
    RegisterComponent,
    MustMatchDirective,

    ReclamationComponent,
    PassagerPipe,
    ConducteurPipe,
    VoyagePipe,
    PaiementComponent,


    MarquevoitureComponent,

    ProfileadminComponent,

    TrajetComponent,
    ReservationComponent,
    ReservationComponent,

    ProfilUserComponent,

    AnnonceComponent



  ],
  imports: [NgbModalModule,BrowserAnimationsModule,  FlatpickrModule.forRoot(),CalendarModule.forRoot({
    provide: DateAdapter,
    useFactory: adapterFactory
  }),NgxPaginationModule,ChartsModule,StarRatingModule.forRoot(),
    RouterModule,  BrowserModule, DatePickerModule,
   AppRoutingModule,FormsModule,HttpClientModule,ReactiveFormsModule, BsDatepickerModule.forRoot()

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
