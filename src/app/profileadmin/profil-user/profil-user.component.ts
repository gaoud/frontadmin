import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ConducteurserviceService} from '../../service';
import {PassagerService} from '../../service/passager.service';

@Component({
  selector: 'app-profil-user',
  templateUrl: './profil-user.component.html',
  styleUrls: ['./profil-user.component.css']
})
export class ProfilUserComponent implements OnInit {
id;
ispassager=true;
isconducteur=true;
user;
  constructor( private activaterout:ActivatedRoute,private conducteurservice:ConducteurserviceService,private passagerservice:PassagerService) {
    this.id=this.activaterout.params['_value']['id'];


  }

  ngOnInit() {
    this.getconducteurbyid(this.id)
  }

getconducteurbyid(id){
    this.conducteurservice.getbyid(id).subscribe(res=>{
      console.log(res);
this.user = res;

this.ispassager=false;
    })

}
  passagerbyid(id){
    this.passagerservice.getone(id).subscribe(res=>{
      console.log(res);
      this.user = res;
      this.ispassager=!this.ispassager;


    })

  }
}
