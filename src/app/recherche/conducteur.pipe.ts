import { Pipe, PipeTransform } from '@angular/core';
import {Conducteur} from '../model/conducteur';

@Pipe({
  name: 'conducteur'
})
export class ConducteurPipe implements PipeTransform {


  transform(value: any, term: any): any {

    if (term == null) {
      return value;
    } else {
      return value.filter(item => (item.username.includes(term)));

    }


  }
}
