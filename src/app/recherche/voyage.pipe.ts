import { Pipe, PipeTransform } from '@angular/core';
import {Conducteur} from '../model/conducteur';
import {Reclamation} from '../model/reclamation';

@Pipe({
  name: 'voyage'
})
export class VoyagePipe implements PipeTransform {


  transform(value: any, term: any): any {
    console.log(term);
    if (term == null) {
      return value;
    } else {
      return value.filter(item => (item.titre.includes(term)));

    }


  }
}
