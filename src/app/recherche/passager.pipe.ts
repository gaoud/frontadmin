import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'passager'
})
export class PassagerPipe implements PipeTransform {

  transform(value: any, term: any): any {

    if (term == null) {
      return value;
    } else {
      return value.filter(item => (item.username.includes(term)));

    }

  }
  }
