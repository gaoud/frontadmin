import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import Swal from "sweetalert2";
import {VoitureService} from '../service/voiture.service';
import {Marquevoiture} from '../model/marquevoiture';
import {ConducteurserviceService} from '../service';
import {ImageService} from '../service/image.service';
import {Modelvoiture} from '../model/modelvoiture';

@Component({
  selector: 'app-marquevoiture',
  templateUrl: './marquevoiture.component.html',
  styleUrls: ['./marquevoiture.component.css']
})
export class MarquevoitureComponent implements OnInit {
  listmodel;
  addMarque:FormGroup ;
  addModel:FormGroup ;
  listmarques;
idmarque1;
  submitted = false;
  id;
  logo;
  myVariable = true;
  filesToUpload: Array<File>;
marquevoiture=new Marquevoiture();
model=new Modelvoiture();
  constructor( private formBuilder: FormBuilder,private voitureservice:VoitureService,private imsr:ImageService) {
    this.all();
  }


  ngOnInit() {
    this.addMarque = this.formBuilder.group({
      nom: ['', [Validators.required]]});

    this.addModel = this.formBuilder.group({
      nom: ['', [Validators.required]]});

  }

  get f() {
    return this.addMarque.controls;
  }


  all() {

    this.voitureservice.getall().subscribe(res => {
        console.log(res);
        this.listmarques=res
      }
    );
  }



  recuper(id,nom,logo) {

    this.id = id;
    this.marquevoiture.nom= nom;
    this.logo=logo;



  }

  getmarquebymodel(idmarque){
    this.voitureservice.getmodelbymarque(idmarque).subscribe( (data:any)=>{
      console.log(data);
this.idmarque1=idmarque;
console.log(this.idmarque1);
      this.listmodel=data;
      this.myVariable = !this.myVariable;


    })
  }


  modifiemodel() {
    const data= {
      nom: this.marquevoiture.nom,
      logo: this.logo}
    console.log(data);


    this.voitureservice.modifmarque(this.id ,data).subscribe(res => {
      console.log(res);

      if (this.filesToUpload != undefined) {

        this.imsr.pushFileToStorage(this.filesToUpload[0]).subscribe(rest => {
          console.log(rest);})}

      // if (event.type === HttpEventType.UploadProgress) {
      //   this.progress.percentage = Math.round(100 * event.loaded / event.total);
      // } else if (event instanceof HttpResponse) {
      //   console.log('File is completely uploaded!');
      // }

      this.all()
    });


  }
  closemodal1() {
    this.marquevoiture.nom = '';
    this.logo="choisir une image"

  }
  closemodal2() {
    this.model.nom = '';
    this.myVariable = !this.myVariable;

  }
  ajout() {
    const data={

      nom: this.marquevoiture.nom,

      logo:this.filesToUpload[0].name}


    this.voitureservice.addmarque(data).subscribe(res => {
      console.log(res);
      this.submitted = true;
      this.imsr.pushFileToStorage(this.filesToUpload[0]).subscribe(rest =>{
        console.log(rest)
        // if (event.type === HttpEventType.UploadProgress) {
        //   this.progress.percentage = Math.round(100 * event.loaded / event.total);
        // } else if (event instanceof HttpResponse) {
        //   console.log('File is completely uploaded!');
        // }
      })
      Swal.fire({
        position: 'top-end',
        type: 'success',
        title: 'Marque  Bien Ajouter',

        timer: 1500
      })
      this.all();
     this.marquevoiture.nom="";
      this.logo= "choisir une image"
    });

    if (this.addMarque.invalid) {
      return;
    }

  }

  delete(id) {
    Swal.fire({
      title: 'Vous  êtes sûre?',
      text: "Vous ne pourrez pas revenir en arrière!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Oui, Supprimer!'
    }).then((result) => {
      if (result.value) {

        this.voitureservice.delete(id).subscribe(res => {

          this.all();

          Swal.fire(
            'Supprimer!',
            'Marque Bien Supprimer ',
            'success'
          )
        });
      }
    })
  }
  recuperFile(file){
    this.filesToUpload = <Array<File>>file.target.files;

    this.logo = file.target.files[0]['name'];
  }
  ajoutmodel(idmarque){
    const data={

      nom: this.model.nom};
         this.voitureservice.addmodel(idmarque,data).subscribe(res=>{
           console.log(res);
         })
  }
}




